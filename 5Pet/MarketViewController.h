//
//  MarketViewController.h
//  5Pet
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketViewController : UIViewController < UITableViewDataSource , UITableViewDelegate >
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *tbl_pet_infor;

@end
