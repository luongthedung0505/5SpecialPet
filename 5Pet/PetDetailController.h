//
//  PetDetailController.h
//  5Pet
//
//  Created by LuongTheDung on 2/26/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetDetailController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tbl_petDetail;

@end
