//
//  MarketViewController.m
//  5Pet
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import "MarketViewController.h"
#import "PetInformationCell.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"

@interface MarketViewController ()
@property NSMutableArray *lst_informationPet ;
@end

@implementation MarketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lst_informationPet = [[NSMutableArray alloc] initWithObjects:@"Sản phẩm cho chó",@"Sản phẩm cho mèo",@"Sản phẩm chim kiểng", @"Sản phẩm cá kiểng", @"Sản phẩm khác", nil] ;
    
    self.tbl_pet_infor.delegate = self ;
    self.tbl_pet_infor.dataSource = self ;
    self.tbl_pet_infor.estimatedRowHeight = 44 ;
    self.tbl_pet_infor.rowHeight = UITableViewAutomaticDimension;
    UINib *nib = [UINib nibWithNibName:@"PetInformationCell" bundle:nil];
    [self.tbl_pet_infor registerNib:nib forCellReuseIdentifier:@"PetInformationCell"];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PetInformationCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PetInformationCell"] ;
    cell.lbl_text_detail.text = [_lst_informationPet objectAtIndex:indexPath.row] ;
    cell.img_thumb.layer.borderWidth = 3.0f ;
    cell.img_thumb.layer.borderColor = [UIColor whiteColor].CGColor ;
    return  cell ;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5 ;
}
#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [reader stopScanning];
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QRCodeReader" message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
