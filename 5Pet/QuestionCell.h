//
//  QuestionCell.h
//  demo
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 gerinn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nameUser;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Datetime;
@property (weak, nonatomic) IBOutlet UILabel *lbl_question;
@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sharing;

@end
