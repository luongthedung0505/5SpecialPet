//
//  PetDetailController.m
//  5Pet
//
//  Created by LuongTheDung on 2/26/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import "PetDetailController.h"
#import "PetInformationDetailCell.h"

@interface PetDetailController () <UITableViewDelegate , UITableViewDataSource >
@property NSMutableArray *lst_informationPet ;
@end

@implementation PetDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tbl_petDetail.delegate = self ;
    self.tbl_petDetail.dataSource = self ;
    self.tbl_petDetail.estimatedRowHeight = 40;
    self.tbl_petDetail.rowHeight = UITableViewAutomaticDimension;
    UINib *nib = [UINib nibWithNibName:@"PetInformationDetailCell" bundle:nil];
    [self.tbl_petDetail registerNib:nib forCellReuseIdentifier:@"PetInformationDetailCell"];

    
    // Do any additional setup after loading the view.
}
-(void)viewDidDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = true ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PetInformationDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PetInformationDetailCell"] ;
//    cell.lbl_text_detail.text = [_lst_informationPet objectAtIndex:indexPath.row] ;
//    cell.img_thumb.layer.borderWidth = 3.0f ;
//    cell.img_thumb.layer.borderColor = [UIColor whiteColor].CGColor ;
    return  cell ;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
