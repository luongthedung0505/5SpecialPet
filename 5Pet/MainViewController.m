//
//  MainViewController.m
//  demo
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 gerinn. All rights reserved.
//

#import "MainViewController.h"
#import "QuestionCell.h"
#import "LeftMenuViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
- (IBAction)btnMenu:(id)sender {
    NSLog(@"dunglt");
  
    ((LeftMenuViewController *)[SlideNavigationController sharedInstance].leftMenu).slideOutAnimationEnabled = TRUE;
    
    [[SlideNavigationController sharedInstance]openMenu:MenuLeft withCompletion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tblQuestionView.delegate = self ;
    self.tblQuestionView.dataSource = self ;
    self.tblQuestionView.estimatedRowHeight = 44 ;
    self.tblQuestionView.rowHeight = UITableViewAutomaticDimension;
    UINib *nib = [UINib nibWithNibName:@"QuestionCell" bundle:nil];
    [self.tblQuestionView registerNib:nib forCellReuseIdentifier:@"QuestionCell"];
   // self.uni_home.title = @"Trang chủ" ;
    //self.tblQuestionView
    
    // Do any additional setup after loading the view.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuestionCell * cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionCell"] ;
    cell.lbl_question.text = @"Mua pet ở đâu thì rẻ và cute ?" ;
    cell.thumbImage.layer.borderWidth = 3.0f ;
    cell.thumbImage.layer.borderColor = [UIColor whiteColor].CGColor ;
    return  cell ;
    
}
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 15 ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark SlideNavigationController Methods
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
//{
//    return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
