//
//  PetViewController.m
//  demo
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 gerinn. All rights reserved.
//

#import "PetViewController.h"
#import "PetInformationCell.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "PetDetailController.h"

@interface PetViewController () <UITableViewDelegate  , UITableViewDataSource , QRCodeReaderDelegate >
@property (weak, nonatomic) IBOutlet UINavigationItem *uni_petView;
@property (weak, nonatomic) IBOutlet UITableView *tbl_list_pet;
@property NSMutableArray *lst_informationPet ;
@end

@implementation PetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lst_informationPet = [[NSMutableArray alloc] initWithObjects:@"DANH SÁCH PET",@"QUÉT BLUETOOTH",@"QUÉT QR CODE", @"BẢN ĐỒ 5PET", nil] ;
 
    self.tbl_list_pet.delegate = self ;
    self.tbl_list_pet.dataSource = self ;
    self.tbl_list_pet.estimatedRowHeight = 44 ;
    self.tbl_list_pet.rowHeight = UITableViewAutomaticDimension;
    UINib *nib = [UINib nibWithNibName:@"PetInformationCell" bundle:nil];
    [self.tbl_list_pet registerNib:nib forCellReuseIdentifier:@"PetInformationCell"];
    
    // Do any additional setup after loading the view.
}
- (void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = true ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PetInformationCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PetInformationCell"] ;
    cell.lbl_text_detail.text = [_lst_informationPet objectAtIndex:indexPath.row] ;
    cell.img_thumb.layer.borderWidth = 3.0f ;
    cell.img_thumb.layer.borderColor = [UIColor whiteColor].CGColor ;
    return  cell ;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 )
    {
        PetDetailController *petDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PetDetailController"] ;
        [self.navigationController pushViewController:petDetail animated:NO];
        self.navigationController.navigationBarHidden = false ;
    }
    if (indexPath.row == 2)
    {
        if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
            static QRCodeReaderViewController *vc = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
                vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
                vc.modalPresentationStyle = UIModalPresentationFormSheet;
            });
            vc.delegate = self;
            
            [vc setCompletionWithBlock:^(NSString *resultAsString) {
                NSLog(@"Completion with result: %@", resultAsString);
            }];
            
            [self presentViewController:vc animated:YES completion:NULL];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Reader not supported by the current device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
    }
}
- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [reader stopScanning];
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QRCodeReader" message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
