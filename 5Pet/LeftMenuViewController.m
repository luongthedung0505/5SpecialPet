//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "ProfileImageCell.h"
#import "InformationCell.h"

@implementation LeftMenuViewController

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.tableView.separatorColor = [UIColor lightGrayColor];
    self.tableView.delegate = self ;
    self.tableView.dataSource = self ;
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
	self.tableView.backgroundView = imageView;
    self.tableView.estimatedRowHeight = 80  ;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    UINib *nib = [UINib nibWithNibName:@"ProfileImageCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"ProfileImageCell"];
    UINib *nibInfo = [UINib nibWithNibName:@"InformationCell" bundle:nil];
    [self.tableView registerNib:nibInfo forCellReuseIdentifier:@"InformationCell"];
  _arr_data = [[NSMutableArray alloc] initWithObjects:@"Thiết lập",@"Giới thiệu bạn bè",@"Đánh giá ứng dụng",@"Phản hồi", @"Thông tin ứng dụng",@"Đổi mật khẩu", @"Thoát",nil] ;

}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==0)
    {
        ProfileImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileImageCell"];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else
    {
        InformationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.lbl_text_infor.text = [_arr_data objectAtIndex:indexPath.row-1] ;
        return cell;
    }

	
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone"
//															 bundle: nil];
//	
//	UIViewController *vc ;
//	
//	switch (indexPath.row)
//	{
//		case 0:
//			vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
//			break;
//			
//		case 1:
//			vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"ProfileViewController"];
//			break;
//			
//		case 2:
//			vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"FriendsViewController"];
//			break;
//			
//		case 3:
//			[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
//			[[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
//			return;
//			break;
//	}
//	
//	[[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
//															 withSlideOutAnimation:self.slideOutAnimationEnabled
//																	 andCompletion:nil];
}

@end
