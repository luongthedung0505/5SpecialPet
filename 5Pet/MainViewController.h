//
//  MainViewController.h
//  demo
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 gerinn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface MainViewController : UIViewController <UITableViewDataSource , UITableViewDelegate , UITabBarDelegate , SlideNavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblQuestionView;



@end
