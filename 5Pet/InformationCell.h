//
//  InformationCell.h
//  5Pet
//
//  Created by LuongTheDung on 2/25/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_thumb;
@property (weak, nonatomic) IBOutlet UILabel *lbl_text_infor;

@end
