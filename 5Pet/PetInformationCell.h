//
//  PetInformationCell.h
//  5Pet
//
//  Created by LuongTheDung on 2/18/17.
//  Copyright © 2017 LuongTheDung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetInformationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_thumb;
@property (weak, nonatomic) IBOutlet UILabel *lbl_text_detail;

@end
